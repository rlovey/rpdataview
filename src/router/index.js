import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login',
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../components/login.vue'),
  },
  // 普通管理
  {
    path: '/rpmain',
    name: 'rpmain',
    component: () => import('../components/rpmain.vue'),
    children: [
      {
        path: '/userinfo',
        name: 'userinfo',
        component: () => import('../components/userinfo/userinfo.vue'),
      },
      {
        path: '/tableset',
        name: 'tableset',
        component: () => import('../components/tableset/index.vue'),
      },
    ],
  },
  // 超级管理
  // {
  //   path: '/super',
  //   name: 'super',
  //   component: () => import('../components/super.vue'),
  //   children: [
  //     {
  //       path: '/userinfo',
  //       name: 'userinfo',
  //       component: () => import('../components/userinfo/userinfo.vue'),
  //     },
  //     {
  //       path: '/tableset',
  //       name: 'tableset',
  //       component: () => import('../components/tableset/index.vue'),
  //     },
  //   ],
  // },
]

const router = new VueRouter({
  routes,
})

export default router
